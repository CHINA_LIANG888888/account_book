package com.codeworker.app.ui.wrapper;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.codeworker.app.R;

public class MyPopupWindow extends PopupWindow {
    private final Activity activity;
    private final static int MAX_POPUP_WINDOW_HEIGHT = 800;
    public final static int MAX_POPUP_WINDOW_ITEMS_SIZE = 5;

    public MyPopupWindow(View view, Activity activity, boolean isFixedHeight) {
        this.activity = activity;
        //背景透明化
        Window window = activity.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        //透明度
        lp.alpha = 0.4f;
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        //设置弹窗
        this.setAnimationStyle(R.style.AnimBottom);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setContentView(view);
        this.setOutsideTouchable(false);
        this.setFocusable(true);
        this.setTouchable(true);
        this.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        if(isFixedHeight) {
            this.setHeight(MAX_POPUP_WINDOW_HEIGHT);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        //消除背景透明化效果
        Window window = activity.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = 1f;
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setAttributes(lp);
    }
}
