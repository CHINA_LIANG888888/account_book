package com.codeworker.app.ui.wrapper;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.codeworker.app.R;
import com.codeworker.app.data.common.Constant;
import com.codeworker.app.utils.UiUtils;

public class MyBaseActivity extends AppCompatActivity {

    private SharedPreferences spData;

    private boolean mIsActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        spData = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
    }

    /**
     * 设置Toolbar，需要在设置view之后
     *
     * @param activity 当前活动
     */
    public void setToolbar(Activity activity) {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        TextView tvBarTitle = activity.findViewById(R.id.tv_toolbar_title);
        //去除toolbar默认的title，设置tv的title
        toolbar.setTitle("");
        tvBarTitle.setText(activity.getTitle());
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (UiUtils.isAppOnBackground(this.getApplication())) {
            mIsActive = false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onRestart() {
        super.onRestart();
        if (!mIsActive && spData.getString("settingPassword", Constant.CLOSE_FINGER_PROTECT)
                .equals(Constant.START_FINGER_PROTECT)) {
            mIsActive = true;
            UiUtils.doAuthentication(this);
        }
    }

}
