package com.codeworker.app.ui.property;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.codeworker.app.R;
import com.codeworker.app.data.entity.Property;
import com.codeworker.app.data.repository.PropertyRepository;
import com.codeworker.app.databinding.ActivityPropertyFormBinding;
import com.codeworker.app.ui.wrapper.MyBaseActivity;
import com.codeworker.app.ui.wrapper.MyTextWatcher;
import com.codeworker.app.utils.FormatUtil;

public class ModifyPropertyActivity extends MyBaseActivity {

    private EditText etPropertyName;
    private EditText etPropertyRemark;
    private Button btnHandleProperty;
    private Property property;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.codeworker.app.databinding.ActivityPropertyFormBinding binding = ActivityPropertyFormBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        setToolbar(this);
        //页面初始化
        initPropertyInfo(view);
        //监听数据变化
        dataChangeListener();
        //监听按钮点击事件
        btnHandleProperty.setOnClickListener(v -> {
            PropertyRepository propertyRepository = new PropertyRepository(getApplication());
            propertyRepository.update(property);
            finish();
        });
    }

    private void initPropertyInfo(View view) {
        //获取前置页面的参数
        Intent intent = getIntent();
        property = (Property) intent.getExtras().getSerializable(PropertyListAdapter.ITEM_DATA);

        etPropertyName = view.findViewById(R.id.et_property_name);
        EditText etPropertyValue = view.findViewById(R.id.et_property_value);
        etPropertyRemark = view.findViewById(R.id.et_property_remark);
        etPropertyValue.setEnabled(false);
        TextView tvPropertyTip = view.findViewById(R.id.tv_property_tip);
        tvPropertyTip.setVisibility(View.VISIBLE);

        etPropertyName.setHint(property.getName());
        etPropertyValue.setHint(FormatUtil.format(property.getValue()*1.0/100));
        String remark = property.getRemark();
        etPropertyRemark.setHint(remark.trim().length() > 0 ? remark : "(空)");

        btnHandleProperty = view.findViewById(R.id.btn_handle_property);
        btnHandleProperty.setEnabled(false);

    }

    private void dataChangeListener() {
        etPropertyName.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                btnHandleProperty.setEnabled(true);
                property.setName(etPropertyName.getText().toString());
            }
        });
        etPropertyRemark.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                btnHandleProperty.setEnabled(true);
                property.setRemark(etPropertyRemark.getText().toString());
            }
        });
    }
}
