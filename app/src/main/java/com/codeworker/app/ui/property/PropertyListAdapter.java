package com.codeworker.app.ui.property;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.codeworker.app.R;
import com.codeworker.app.data.entity.Property;
import com.codeworker.app.utils.FormatUtil;

import java.util.ArrayList;
import java.util.List;


public class PropertyListAdapter extends ListAdapter<Property, PropertyListAdapter.PropertyViewHolder> {
    public static final String ITEM_DATA = "ITEM_DATA";

    public PropertyListAdapter(@NonNull DiffUtil.ItemCallback<Property> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public PropertyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return PropertyViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(PropertyViewHolder holder, int position) {
        Property current = getItem(position);
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), ModifyPropertyActivity.class);
            intent.putExtra(ITEM_DATA, current);
            view.getContext().startActivity(intent);
        });
        holder.bind(current);
    }

    @Override
    public void submitList(final List<Property> list) {
        super.submitList(list != null ? new ArrayList<>(list) : null);
    }

    static class ItemDiff extends DiffUtil.ItemCallback<Property> {

        @Override
        public boolean areItemsTheSame(@NonNull Property oldItem, @NonNull Property newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Property oldItem, @NonNull Property newItem) {
            return oldItem.getName().equals(newItem.getName());
        }
    }


    static class PropertyViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvPropertyName;
        private final TextView tvPropertyValue;

        private PropertyViewHolder(View itemView) {
            super(itemView);
            tvPropertyName = itemView.findViewById(R.id.tv_common_view_item_name);
            tvPropertyValue = itemView.findViewById(R.id.tv_common_view_item_value);
            LinearLayout llIcon = itemView.findViewById(R.id.ll_common_view_item_icon);
            llIcon.setVisibility(View.GONE);
        }

        public void bind(Property property) {
            tvPropertyName.setText(property.getName());
            tvPropertyValue.setText(FormatUtil.format(property.getValue()*1.0/100));
        }

        static PropertyViewHolder create(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_common_view_item, parent, false);
            return new PropertyViewHolder(view);
        }
    }


}
