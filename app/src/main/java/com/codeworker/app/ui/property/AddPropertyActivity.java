package com.codeworker.app.ui.property;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.codeworker.app.R;
import com.codeworker.app.data.entity.Property;
import com.codeworker.app.databinding.ActivityPropertyFormBinding;
import com.codeworker.app.ui.wrapper.MyBaseActivity;
import com.codeworker.app.ui.wrapper.MyTextWatcher;

public class AddPropertyActivity extends MyBaseActivity {
    public static final String EXTRA_REPLY = "extra_reply_property";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.codeworker.app.databinding.ActivityPropertyFormBinding binding = ActivityPropertyFormBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        setToolbar(this);

        final EditText etPropertyName = findViewById(R.id.et_property_name);
        final EditText etPropertyValue = findViewById(R.id.et_property_value);
        final EditText etPropertyRemark = findViewById(R.id.et_property_remark);
        final Button button = findViewById(R.id.btn_handle_property);
        etPropertyValue.addTextChangedListener(new MyTextWatcher());
        button.setOnClickListener(v -> {
            if (TextUtils.isEmpty(etPropertyName.getText()) || TextUtils.isEmpty(etPropertyValue.getText())) {
                Toast.makeText(v.getContext(), R.string.toast_must_not_null, Toast.LENGTH_SHORT).show();
                return;
            }
            Intent replyIntent = new Intent();
            String name = etPropertyName.getText().toString();
            int value = (int) (Double.parseDouble(etPropertyValue.getText().toString()) * 100);
            String remark = etPropertyRemark.getText().toString();
            Property property = new Property(name, value, remark);
            replyIntent.putExtra(EXTRA_REPLY, property);
            setResult(RESULT_OK, replyIntent);
            finish();
        });
    }

}
