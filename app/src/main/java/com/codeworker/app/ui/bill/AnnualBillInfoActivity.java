package com.codeworker.app.ui.bill;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.codeworker.app.R;
import com.codeworker.app.data.entity.MonthlyRecord;
import com.codeworker.app.data.repository.BillRepository;
import com.codeworker.app.databinding.ActivityBillInfoBinding;
import com.codeworker.app.ui.wrapper.MyBaseActivity;
import com.codeworker.app.utils.FormatUtil;

import java.time.LocalDate;
import java.util.List;

public class AnnualBillInfoActivity extends MyBaseActivity {

    private TextView tvToolbarTitle;
    private TextView tvInfoTitle;
    private TextView tvBalanceTitle;
    private TextView tvMonthlyInfo;
    private TextView tvIncome;
    private TextView tvExpend;
    private TextView tvBalance;
    private RecyclerView rvMonthly;
    private BillRepository billRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBillInfoBinding binding = ActivityBillInfoBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        setContentView(root);
        setToolbar(this);
        initView(root);
        initBaseData();
    }

    private void initView(View root) {
        tvToolbarTitle = root.findViewById(R.id.tv_toolbar_title);
        tvInfoTitle = root.findViewById(R.id.tv_bill_info_title);
        tvBalanceTitle = root.findViewById(R.id.tv_bill_info_title3);
        tvMonthlyInfo = root.findViewById(R.id.tv_monthly_bill_rank_title);
        tvIncome = root.findViewById(R.id.tv_bill_info1);
        tvExpend = root.findViewById(R.id.tv_bill_info2);
        tvBalance = root.findViewById(R.id.tv_bill_info3);
        LinearLayout llProportion = root.findViewById(R.id.ll_proportion);
        TextView tvMore = root.findViewById(R.id.tv_monthly_bill_more_title);
        rvMonthly = root.findViewById(R.id.rv_monthly_bill_rank);

        tvBalanceTitle.setVisibility(View.VISIBLE);
        tvBalance.setVisibility(View.VISIBLE);
        tvMore.setVisibility(View.GONE);
        llProportion.setVisibility(View.GONE);
    }

    private void initBaseData() {
        int year = getIntent().getIntExtra(BillFragment.YEAR_DATA, LocalDate.now().getYear());
        String title = year + "年账单";
        tvToolbarTitle.setText(title);
        tvInfoTitle.setText(title);
        tvBalanceTitle.setText(R.string.text_annual_bill_balance);

        billRepository = new BillRepository(getApplication());
        List<MonthlyRecord> monthlyRecordList = billRepository.getMonthlyRecordList(year);
        tvMonthlyInfo.setText(R.string.text_monthly_bill_info_detail);
        setBalanceData(monthlyRecordList.isEmpty());

        BillMonthlyListAdapter billMonthlyListAdapter = new BillMonthlyListAdapter(new BillMonthlyListAdapter.ItemDiff());
        billMonthlyListAdapter.submitList(monthlyRecordList);
        rvMonthly.setAdapter(billMonthlyListAdapter);
    }

    @SuppressLint("SetTextI18n")
    private void setBalanceData(boolean withoutData) {
        if (withoutData) {
            tvExpend.setText("0.00");
            tvIncome.setText("0.00");
            tvBalance.setText("0.00");
            return;
        }
        int totalIncome = billRepository.getMonthlyRecordList().stream().mapToInt(MonthlyRecord::getIncome).sum();
        int totalExpend = billRepository.getMonthlyRecordList().stream().mapToInt(MonthlyRecord::getExpend).sum();
        tvExpend.setText(FormatUtil.format(totalExpend * 1.0 / 100));
        tvIncome.setText(FormatUtil.format(totalIncome * 1.0 / 100));
        tvBalance.setText(FormatUtil.format((totalIncome - totalExpend) * 1.0 / 100));
    }

}
