package com.codeworker.app.ui.bill;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.codeworker.app.R;
import com.codeworker.app.data.entity.vo.BillTypeRank;

import java.util.ArrayList;
import java.util.List;


public class BillMonthlyTypeListAdapter extends ListAdapter<BillTypeRank, BillMonthlyTypeListAdapter.BillTypeViewHolder> {

    public BillMonthlyTypeListAdapter(@NonNull DiffUtil.ItemCallback<BillTypeRank> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public BillTypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return BillTypeViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(BillTypeViewHolder holder, int position) {
        BillTypeRank current = getItem(position);
        holder.bind(current);
    }

    @Override
    public void submitList(final List<BillTypeRank> list) {
        super.submitList(list != null ? new ArrayList<>(list) : null);
    }

    static class ItemDiff extends DiffUtil.ItemCallback<BillTypeRank> {

        @Override
        public boolean areItemsTheSame(@NonNull BillTypeRank oldItem, @NonNull BillTypeRank newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull BillTypeRank oldItem, @NonNull BillTypeRank newItem) {
            return oldItem.getBillType().equals(newItem.getBillType());
        }
    }


    static class BillTypeViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvColor;
        private final TextView tvType;
        private final TextView tvProportion;
        private final TextView tvMoney;
        private final TextView tvTimes;

        private BillTypeViewHolder(View itemView) {
            super(itemView);
            tvColor = itemView.findViewById(R.id.tv_monthly_bill_color);
            tvType = itemView.findViewById(R.id.tv_monthly_bill_type);
            tvProportion = itemView.findViewById(R.id.tv_monthly_bill_proportion);
            tvMoney = itemView.findViewById(R.id.tv_monthly_bill_money);
            tvTimes = itemView.findViewById(R.id.tv_monthly_bill_times);
        }

        @SuppressLint("SetTextI18n")
        public void bind(BillTypeRank billTypeRank) {
            tvColor.setBackgroundColor(Color.parseColor(billTypeRank.getBillTypeColor()));
            tvType.setText(billTypeRank.getBillType());
            tvProportion.setText(billTypeRank.getProportion()+"%");
            tvMoney.setText(billTypeRank.getMoney());
            tvTimes.setText(String.valueOf(billTypeRank.getTimes()));
        }

        static BillTypeViewHolder create(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_bill_proportion_info, parent, false);
            return new BillTypeViewHolder(view);
        }
    }


}
