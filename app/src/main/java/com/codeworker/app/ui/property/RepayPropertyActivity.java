package com.codeworker.app.ui.property;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.codeworker.app.R;
import com.codeworker.app.data.entity.Property;
import com.codeworker.app.data.repository.PropertyRepository;
import com.codeworker.app.databinding.ActivityPropertyRepayBinding;
import com.codeworker.app.ui.wrapper.MyBaseActivity;
import com.codeworker.app.ui.wrapper.MyPopupWindow;
import com.codeworker.app.ui.wrapper.MyPopupWindowAdapter;

import java.util.ArrayList;
import java.util.List;

public class RepayPropertyActivity extends MyBaseActivity {
    public final static String EXTRA_REPLY_INPUT = "IN_ID";
    public final static String EXTRA_REPLY_OUTPUT = "OUT_ID";
    public final static String EXTRA_REPLY_MONEY = "MONEY";
    private PropertyRepository propertyRepository;
    private List<MyPopupWindowAdapter.PopupWindowViewHolderInDB> popupWindowItems;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityPropertyRepayBinding binding = ActivityPropertyRepayBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        setToolbar(this);
        propertyRepository = new PropertyRepository(this.getApplication());
        //TODO model复用
//        propertyModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(PropertyModel.class);
//        Log.d("---------", propertyModel.getPropertyListLiveData().getValue().size() + "");
        final TextView tvInProperty = findViewById(R.id.tv_input_property);
        final TextView tvInPropertyId = findViewById(R.id.tv_input_property_id);
        final TextView tvOutProperty = findViewById(R.id.tv_output_property);
        final TextView tvOutPropertyId = findViewById(R.id.tv_output_property_id);
        final EditText etPropertyMoney = findViewById(R.id.et_property_money);
        final Button button = findViewById(R.id.btn_repay_property);
        tvInProperty.setOnClickListener(v -> popupWindowEvent(v, tvInProperty, tvInPropertyId));
        tvOutProperty.setOnClickListener(v -> popupWindowEvent(v, tvOutProperty, tvOutPropertyId));
        button.setOnClickListener(v -> {
            if (TextUtils.isEmpty(tvInProperty.getText()) ||
                    TextUtils.isEmpty(tvOutProperty.getText()) ||
                    TextUtils.isEmpty(etPropertyMoney.getText())) {
                Toast.makeText(v.getContext(), R.string.toast_must_not_null, Toast.LENGTH_SHORT).show();
                return;
            }
            int inId = Integer.parseInt(tvInPropertyId.getText().toString());
            int outId = Integer.parseInt(tvOutPropertyId.getText().toString());
            if (inId == outId) {
                Toast.makeText(v.getContext(), R.string.toast_invalid_handle, Toast.LENGTH_SHORT).show();
                return;
            }

            Intent replyIntent = new Intent();
            replyIntent.putExtra(EXTRA_REPLY_INPUT, inId);
            replyIntent.putExtra(EXTRA_REPLY_OUTPUT, outId);
            replyIntent.putExtra(EXTRA_REPLY_MONEY, (int) (Double.parseDouble(etPropertyMoney.getText().toString())*100));
            setResult(RESULT_OK, replyIntent);
            finish();
        });
    }

    //设置弹窗事件
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void popupWindowEvent(View v, TextView tv, TextView tvId) {
        @SuppressLint("InflateParams") View popUpView
                = LayoutInflater.from(v.getContext()).inflate(R.layout.layout_common_popup, null);
        //设置适配器的数据项
        ListView listView = popUpView.findViewById(R.id.lv_common_popup);
        popupWindowItems = new ArrayList<>();
        for (Property property : propertyRepository.getProperties()) {
            popupWindowItems.add(new MyPopupWindowAdapter.PopupWindowViewHolderInDB(property.getName(), property.getId()));
        }

        //初始化适配器
        MyPopupWindowAdapter adapter = new MyPopupWindowAdapter(v.getContext(), popupWindowItems);
        listView.setAdapter(adapter);

        //设置弹窗
        boolean isFixedPopupWindowHeight = popupWindowItems.size() > MyPopupWindow.MAX_POPUP_WINDOW_ITEMS_SIZE;
        MyPopupWindow myPopupWindow = new MyPopupWindow(popUpView, this, isFixedPopupWindowHeight);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            tv.setText(popupWindowItems.get(position).getItem());
            tvId.setText(String.valueOf(popupWindowItems.get(position).getId()));
            //关闭弹出框
            myPopupWindow.dismiss();
        });
        //弹出创建显示在按钮下面
        myPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
    }
}
