package com.codeworker.app.ui.bill;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.codeworker.app.R;
import com.codeworker.app.data.entity.MonthlyRecord;
import com.codeworker.app.utils.FormatUtil;

import java.util.ArrayList;
import java.util.List;

public class BillMonthlyListAdapter extends ListAdapter<MonthlyRecord, BillMonthlyListAdapter.BillMonthlyViewHolder> {

    public static final String ITEM_DATA = "ITEM_DATA";

    public BillMonthlyListAdapter(@NonNull DiffUtil.ItemCallback<MonthlyRecord> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public BillMonthlyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return BillMonthlyViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull BillMonthlyViewHolder holder, int position) {
        MonthlyRecord current = getItem(position);
        holder.bind(current);
        holder.itemView.findViewById(R.id.tv_monthly_view_item_detail).setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), MonthlyBillInfoActivity.class);
            intent.putExtra(ITEM_DATA, current);
            view.getContext().startActivity(intent);
        });
    }

    @Override
    public void submitList(final List<MonthlyRecord> list) {
        super.submitList(list != null ? new ArrayList<>(list) : null);
    }

    static class ItemDiff extends DiffUtil.ItemCallback<MonthlyRecord> {

        @Override
        public boolean areItemsTheSame(@NonNull MonthlyRecord oldItem, @NonNull MonthlyRecord newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull MonthlyRecord oldItem, @NonNull MonthlyRecord newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    }

    static class BillMonthlyViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvMonthValue;
        private final TextView tvIncome;
        private final TextView tvExpend;

        private BillMonthlyViewHolder(View itemView) {
            super(itemView);
            tvMonthValue = itemView.findViewById(R.id.tv_monthly_view_item_value);
            tvIncome = itemView.findViewById(R.id.tv_monthly_view_item_income);
            tvExpend = itemView.findViewById(R.id.tv_monthly_view_item_expend);
        }

        @SuppressLint("SetTextI18n")
        public void bind(MonthlyRecord monthlyRecord) {
            int income = monthlyRecord.getIncome();
            int expend = monthlyRecord.getExpend();
            tvMonthValue.setText(monthlyRecord.getMonth() + "月");
            tvIncome.setText("+" + FormatUtil.format(income * 1.0 / 100));
            tvExpend.setText("-" + FormatUtil.format(expend * 1.0 / 100));
        }

        static BillMonthlyViewHolder create(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_bill_monthly_view_item, parent, false);
            return new BillMonthlyViewHolder(view);
        }
    }

}
