package com.codeworker.app.ui.bill;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.codeworker.app.R;
import com.codeworker.app.data.common.enums.PayTypeEnum;
import com.codeworker.app.data.entity.Bill;
import com.codeworker.app.data.repository.BillRepository;
import com.codeworker.app.databinding.ActivityBillMonthlyRankInfoBinding;
import com.codeworker.app.ui.wrapper.MyBaseActivity;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BillMonthlyRankInfoActivity extends MyBaseActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBillMonthlyRankInfoBinding binding = ActivityBillMonthlyRankInfoBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        setContentView(root);
        setToolbar(this);

        int payType = getIntent().getExtras().getInt(MonthlyBillInfoActivity.MONTHLY_RECORDS_PAY_TYPE);
        int month = getIntent().getExtras().getInt(MonthlyBillInfoActivity.MONTHLY_RECORDS_MONTH);
        int year = getIntent().getExtras().getInt(MonthlyBillInfoActivity.MONTHLY_RECORDS_YEAR);
        String strPayType = PayTypeEnum.INCOME.getPayTypeInDB().equals(payType) ?
                PayTypeEnum.INCOME.getPayTypeDesc() : PayTypeEnum.EXPEND.getPayTypeDesc();

        TextView tvToolbarTitle = root.findViewById(R.id.tv_toolbar_title);
        tvToolbarTitle.setText(year + "年" + month + "月" + strPayType + "排行");

        BillRepository billRepository = new BillRepository(getApplication());
        List<Bill> monthlyBill = billRepository.getMonthlyBill(month, year).stream()
                .filter(bill -> payType == bill.getPayType())
                .sorted(Comparator.comparing(Bill::getValue).reversed())
                .collect(Collectors.toList());

        BillMonthlyRankListAdapter billMonthlyRankListAdapter = new BillMonthlyRankListAdapter(
                new BillMonthlyRankListAdapter.ItemDiff());
        billMonthlyRankListAdapter.submitList(monthlyBill);

        RecyclerView rvRank = root.findViewById(R.id.rv_monthly_bill_rank);
        rvRank.setAdapter(billMonthlyRankListAdapter);
    }
}
