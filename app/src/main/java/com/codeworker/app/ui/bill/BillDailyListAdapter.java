package com.codeworker.app.ui.bill;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.codeworker.app.R;
import com.codeworker.app.data.common.enums.PayTypeEnum;
import com.codeworker.app.data.entity.DailyRecord;
import com.codeworker.app.data.entity.vo.BillData;
import com.codeworker.app.data.model.BillModel;
import com.codeworker.app.ui.wrapper.MyDecoration;
import com.codeworker.app.utils.FormatUtil;

import java.util.ArrayList;
import java.util.List;


public class BillDailyListAdapter extends ListAdapter<BillData, BillDailyListAdapter.BillViewHolder> {

    public final static String ITEM_DATA = "ITEM_DATA";

    @SuppressLint("StaticFieldLeak")
    public static BillFragment billFragment;

    public BillDailyListAdapter(@NonNull DiffUtil.ItemCallback<BillData> diffCallback, BillFragment billFragment) {
        super(diffCallback);
        BillDailyListAdapter.billFragment = billFragment;
    }

    @NonNull
    @Override
    public BillViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return BillViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull BillViewHolder holder, int position) {
        BillData current = getItem(position);
        DailyRecord dailyRecord = current.getDailyRecord();
        if (dailyRecord.getIncome().equals(dailyRecord.getExpend()) && dailyRecord.getIncome() == 0) {
            holder.bind(null);
        }else {
            holder.bind(current);
        }
        holder.itemView.setOnClickListener(view -> {
            current.setShowDailyBills(!current.isShowDailyBills());
            holder.bind(current);
        });
    }

    @Override
    public void submitList(final List<BillData> list) {
        super.submitList(list != null ? new ArrayList<>(list) : null);
    }

    static class ItemDiff extends DiffUtil.ItemCallback<BillData> {

        @Override
        public boolean areItemsTheSame(@NonNull BillData oldItem, @NonNull BillData newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull BillData oldItem, @NonNull BillData newItem) {
            return oldItem.getDailyRecord().getId().equals(newItem.getDailyRecord().getId());
        }
    }

    static class BillViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvBillDay;
        private final TextView tvBillIncome;
        private final TextView tvBillExpend;
        private final RecyclerView rv;
        private final LinearLayout ll;

        private BillViewHolder(View itemView) {
            super(itemView);
            ll = itemView.findViewById(R.id.ll);
            tvBillDay = itemView.findViewById(R.id.tv_bill_day);
            tvBillIncome = itemView.findViewById(R.id.tv_bill_income);
            tvBillExpend = itemView.findViewById(R.id.tv_bill_expend);
            rv = itemView.findViewById(R.id.rv);
            rv.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
            rv.addItemDecoration(new MyDecoration());
            rv.setNestedScrollingEnabled(false);
        }

        public void setVisibility(int visibility) {
            ll.setVisibility(visibility);
        }

        @SuppressLint({"SetTextI18n", "NotifyDataSetChanged"})
        public void bind(BillData billData) {
            if (billData == null) {
                ll.setVisibility(View.GONE);
                return;
            }
            BillModel billModel = new ViewModelProvider(billFragment).get(BillModel.class);
            DailyRecord dailyRecord = billData.getDailyRecord();
            billModel.setDailyRecordLiveData(dailyRecord.getDay(), dailyRecord.getMonth(), dailyRecord.getYear());
            billModel.getDailyRecordLiveData().observe(billFragment.getViewLifecycleOwner(), data -> {
                tvBillDay.setText(data.getMonth() + "月" + data.getDay() + "日");
                tvBillIncome.setText(PayTypeEnum.INCOME.getPayTypeDesc() + ": "
                        + FormatUtil.format(data.getIncome() * 1.0 / 100));
                tvBillExpend.setText(PayTypeEnum.EXPEND.getPayTypeDesc() + ": "
                        + FormatUtil.format((data.getExpend() * 1.0 / 100)));
            });
            if (billData.isShowDailyBills()) {
                final BillDailySubListAdapter adapter = new BillDailySubListAdapter(
                        new BillDailySubListAdapter.ItemDiff(), billModel, billFragment, this);
                adapter.submitList(billData.getDailyBills());
                rv.setAdapter(adapter);
            } else {
                rv.setAdapter(null);
            }
        }

        static BillViewHolder create(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_bill_daily_view_item, parent, false);
            return new BillViewHolder(view);
        }

    }

}
