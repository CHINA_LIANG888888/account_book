package com.codeworker.app.ui.wrapper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.codeworker.app.R;

import java.util.List;

public class MyPopupWindowAdapter extends BaseAdapter {
    private final List<PopupWindowViewHolderInDB> popupWindowItems;
    private final LayoutInflater inflater;
    private final static int MAX_POPUP_WINDOW_ITEMS_SIZE = 5;
    private final static int MAX_POPUP_WINDOW_HEIGHT = 800;

    public MyPopupWindowAdapter(Context context, List<PopupWindowViewHolderInDB> popupWindowItems) {
        this.popupWindowItems = popupWindowItems;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return popupWindowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return popupWindowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PopupWindowViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_common_popup_item, null);
            holder = new PopupWindowViewHolder();
            holder.popupItem = (TextView) convertView.findViewById(R.id.tv_common_popup_item);
            holder.popupItemId = (TextView) convertView.findViewById(R.id.tv_common_popup_item_id);
            convertView.setTag(holder);
        } else {
            holder = (PopupWindowViewHolder) convertView.getTag();
        }
        holder.popupItem.setText(popupWindowItems.get(position).item);
        holder.popupItemId.setText(String.valueOf(popupWindowItems.get(position).id));
        return convertView;
    }

    //TODO popupWindow复用
    void popupWindowInit(View v, TextView tv, MyPopupWindow myPopupWindow, MyPopupWindowAdapter adapter) {
        @SuppressLint("InflateParams") View popUpView
                = LayoutInflater.from(v.getContext()).inflate(R.layout.layout_common_popup, null);
        //设置适配器的数据项
        ListView listView = popUpView.findViewById(R.id.lv_common_popup);
        listView.setAdapter(adapter);

        //设置弹窗
        listView.setOnItemClickListener((parent, view, position, id) -> {
            tv.setText(popupWindowItems.get(position).item);
            //关闭弹出框
            myPopupWindow.dismiss();
        });
        //如果数据项过多，则固定弹窗高度
        if (popupWindowItems.size() > MAX_POPUP_WINDOW_ITEMS_SIZE) {
            myPopupWindow.setHeight(MAX_POPUP_WINDOW_HEIGHT);
        }
        //弹出创建显示在按钮下面
        myPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
    }

    public static class PopupWindowViewHolder {
        TextView popupItem;
        TextView popupItemId;
    }

    public static class PopupWindowViewHolderInDB {
        String item;
        Integer id;

        public String getItem() {
            return item;
        }

        public void setItem(String item) {
            this.item = item;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public PopupWindowViewHolderInDB(String item, Integer id) {
            this.item = item;
            this.id = id;
        }
    }
}
