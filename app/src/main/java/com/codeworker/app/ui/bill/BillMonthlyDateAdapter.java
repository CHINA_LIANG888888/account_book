package com.codeworker.app.ui.bill;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.codeworker.app.R;

import java.util.ArrayList;
import java.util.List;

public class BillMonthlyDateAdapter extends BaseAdapter {

    private int clickTemp;

    List<String> dateData = new ArrayList<>();

    public BillMonthlyDateAdapter(int defaultClick, boolean isMonthlyData) {
        clickTemp = defaultClick;
        if(isMonthlyData) {
            initMonthlyData();
        }else {
            initAnnualData();
        }
    }

    public void initMonthlyData() {
        for(int i = 1; i <= 12; i++){
            dateData.add(i + "月");
        }
    }

    public void initAnnualData() {
        for(int i = 2022; i <= 2050; i++){
            dateData.add(i + "年");
        }
    }

    public void setSelectedItem(int i) {
        clickTemp = i;
    }

    @Override
    public int getCount() {
        return dateData.size();
    }

    @Override
    public Object getItem(int i) {
        return dateData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        @SuppressLint({"ViewHolder", "InflateParams"}) View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_common_grid_view_item, null);
        TextView tv = v.findViewById(R.id.tv_date);
        tv.setText(dateData.get(i));
        if(clickTemp == i) {
            tv.setBackgroundResource(R.drawable.shape_bar_tv);
            tv.setTextColor(Color.WHITE);
        }else {
            tv.setBackgroundResource(R.drawable.shape_bar_white_tv);
            tv.setTextColor(Color.BLACK);
        }
        return v;
    }
}
