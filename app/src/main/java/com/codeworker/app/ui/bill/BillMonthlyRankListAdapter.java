package com.codeworker.app.ui.bill;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.codeworker.app.R;
import com.codeworker.app.data.common.enums.BillTypeEnum;
import com.codeworker.app.data.entity.Bill;
import com.codeworker.app.utils.FormatUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class BillMonthlyRankListAdapter extends ListAdapter<Bill, BillMonthlyRankListAdapter.BillRankViewHolder> {

    public BillMonthlyRankListAdapter(@NonNull DiffUtil.ItemCallback<Bill> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public BillRankViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return BillRankViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(BillRankViewHolder holder, int position) {
        Bill current = getItem(position);
        holder.bind(current, position + 1);
    }

    @Override
    public void submitList(final List<Bill> list) {
        super.submitList(list != null ? new ArrayList<>(list) : null);
    }

    static class ItemDiff extends DiffUtil.ItemCallback<Bill> {

        @Override
        public boolean areItemsTheSame(@NonNull Bill oldItem, @NonNull Bill newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Bill oldItem, @NonNull Bill newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    }


    static class BillRankViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvPropertyName;
        private final TextView tvPropertyValue;
        private final TextView tvBillRank;
        private final TextView tvBillTime;
        private final ImageView ivPropertyIcon;

        private BillRankViewHolder(View itemView) {
            super(itemView);
            tvPropertyName = itemView.findViewById(R.id.tv_common_view_item_name);
            tvPropertyValue = itemView.findViewById(R.id.tv_common_view_item_value);
            tvBillRank = itemView.findViewById(R.id.tv_common_view_item_rank);
            tvBillRank.setVisibility(View.VISIBLE);
            tvBillTime = itemView.findViewById(R.id.tv_common_view_item_time);
            tvBillTime.setVisibility(View.VISIBLE);
            ivPropertyIcon = itemView.findViewById(R.id.iv_common_view_item_icon);
        }

        @SuppressLint("SetTextI18n")
        public void bind(Bill bill, int index) {
            tvPropertyName.setText(BillTypeEnum.getBillDesc(bill.getPayType(), bill.getType()));
            tvPropertyValue.setText(FormatUtil.format(bill.getValue() * 1.0 / 100));
            LocalDateTime localDateTime = LocalDateTime.parse(bill.getCreateTime(), FormatUtil.formatter);
            tvBillTime.setText(localDateTime.getMonthValue()+"月"+localDateTime.getDayOfMonth()+"日");
            tvBillRank.setText(String.valueOf(index));
            int resId = itemView.getContext().getResources()
                    .getIdentifier(BillTypeEnum.getBillIcon(bill.getPayType(), bill.getType()),
                            "drawable", itemView.getContext().getPackageName());
            ivPropertyIcon.setImageResource(resId);
        }

        static BillRankViewHolder create(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_common_view_item, parent, false);
            return new BillRankViewHolder(view);
        }
    }

}
