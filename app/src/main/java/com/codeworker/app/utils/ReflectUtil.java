package com.codeworker.app.utils;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 反射工具
 * https://blog.csdn.net/sinat_34338162/article/details/118612416
 * https://blog.csdn.net/qq_43511677/article/details/107016251
 */
public class ReflectUtil {
    /**
     * 通过setter设置对象属性值
     *
     * @param fieldName 属性名(字符串形式)
     * @param obj       对象
     */
    public static void setValueBySetter(String fieldName, Object obj, Object value) {
        try {
            //通过反射获取某个声明的属性
            Field declaredField = obj.getClass().getDeclaredField(fieldName);
            //一般属性私有化，通过setAccessible方法，可以免去安全访问环节，对象可以直接访问。
            declaredField.setAccessible(true);
            //通过set方法，修改属性，里面的参数是实例对象和修改后的参数。
            declaredField.set(obj, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过getter获取对象属性值
     *
     * @param fieldName 属性名(字符串形式)
     * @param obj       对象
     * @return 对象属性值
     */
    public static Object getValueByGetter(String fieldName, Object obj) {
        Method getter = getGetter(fieldName, obj.getClass());
        if (getter != null) {
            try {
                return getter.invoke(obj);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 获取get方法
     *
     * @param fieldName 属性名(字符串形式)
     * @param cls       类
     * @return 方法对象
     */
    public static Method getGetter(String fieldName, Class<?> cls) {
        for (Method method : cls.getMethods()) {
            if (method.getName().equalsIgnoreCase("get".concat(fieldName))
                    && method.getParameterTypes().length == 0) {
                return method;
            }
        }
        return null;
    }
}