package com.codeworker.app.utils;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FormatUtil {

    private final static String NUMBER_FORMAT = "0.00";

    private final static String ROUND_NUMBER_FORMAT = "0";

    private static final DecimalFormat df = new DecimalFormat(FormatUtil.NUMBER_FORMAT);

    private static final DecimalFormat df2 = new DecimalFormat(FormatUtil.ROUND_NUMBER_FORMAT);

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static String format(double num) {
        return df.format(num);
    }

    public static String format2(double num) {
        return df2.format(num);
    }

    public static String format(LocalDateTime localDateTime) {
        return formatter.format(localDateTime);
    }
}
