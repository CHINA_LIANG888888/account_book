package com.codeworker.app.utils;

import java.util.Objects;

public class ObjectUtil {

    public static <T> T dataOrDefault(boolean condition, T data, T defaultData) {
        return condition ? data : defaultData;
    }

    public static <T> T dataOrDefault(T data, T defaultData) {
        return dataOrDefault(Objects.nonNull(data) && data != "", data, defaultData);
    }
}
