package com.codeworker.app.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.codeworker.app.data.entity.Property;

import java.util.List;

@Dao
public interface PropertyDao {
    @Query("SELECT * FROM property_table WHERE isDeleted = 0 ORDER BY value DESC")
    LiveData<List<Property>> getPropertyListLiveData();

    @Query("SELECT * FROM property_table WHERE isDeleted = 0 ORDER BY value DESC")
    List<Property> getPropertyList();

    @Query("SELECT * FROM PROPERTY_TABLE WHERE id = :id")
    Property getPropertyById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(Property property);

    @Update
    void update(Property property);

}
