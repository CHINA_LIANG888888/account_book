package com.codeworker.app.data.common;

/**
 * 常量表
 */
public class Constant {
    private Constant() {}
    /**
     * 已删除
     */
    public static final Integer HAS_DELETED = 1;
    /**
     * 未删除
     */
    public static final Integer NOT_DELETED = 0;
    /**
     * 账户已注销
     */
    public static final String DELETED_MSG = "(账户已注销)";

    public static final String START_FINGER_PROTECT = "开启";

    public static final String CLOSE_FINGER_PROTECT = "关闭";
}
