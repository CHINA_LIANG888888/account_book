package com.codeworker.app.data.entity.vo;

/**
 * 月度开支占比
 */
public class BillProportion {
    /**
     * 类型
     */
    private Integer billType;

    /**
     * 总额
     */
    private Integer total;

    /**
     * 次数
     */
    private Integer times;

    public BillProportion(Integer billType, Integer total, Integer times) {
        this.billType = billType;
        this.total = total;
        this.times = times;
    }

    public Integer getBillType() {
        return billType;
    }

    public void setBillType(Integer billType) {
        this.billType = billType;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    @Override
    public String toString() {
        return "BillProportion{" +
                "billType=" + billType +
                ", total=" + total +
                ", times=" + times +
                '}';
    }
}
