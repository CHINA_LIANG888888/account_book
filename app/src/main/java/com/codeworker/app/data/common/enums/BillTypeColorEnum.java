package com.codeworker.app.data.common.enums;

public enum BillTypeColorEnum {
    COLOR_1("#fec143"),
    COLOR_2("#ea5a59"),
    COLOR_3("#8485f9"),
    COLOR_4("#13c3c3"),
    COLOR_5("#1891fc"),
    COLOR_6("#A31ACB"),
    COLOR_7("#1F4690"),
    COLOR_8("#231955"),
    COLOR_9("#a18cd1"),
    COLOR_10("#fbc2eb"),
    COLOR_11("#d4fc79"),
    COLOR_12("#c2e9fb"),
    COLOR_13("#8fd3f4"),
    COLOR_14("#84fab0"),
    COLOR_15("#a1c4fd"),
    COLOR_16("#c2e9fb"),
    COLOR_17("#e2ebf0"),
    COLOR_18("#00f2fe"),
    COLOR_19("#a8edea"),
    COLOR_20("#e2d1c3"),
    ;
    private final String color;

    BillTypeColorEnum(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
