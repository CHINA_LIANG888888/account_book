package com.codeworker.app.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.codeworker.app.data.entity.Bill;

import java.util.List;

@Dao
public interface BillDao {

    @Query("SELECT * FROM bill_table WHERE month = :month and year = :year ORDER BY createTime ASC")
    List<Bill> getBillListByMonth(int month, int year);

    @Query("SELECT * FROM bill_table WHERE day = :day and month = :month and year = :year ORDER BY createTime DESC")
    List<Bill> getBillListByDMY(int day, int month, int year);

    @Query("SELECT * FROM bill_table WHERE day = :day and month = :month and year = :year ORDER BY createTime DESC")
    LiveData<List<Bill>> getBillLiveData(int day,int month, int year);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Bill bill);

    @Delete
    void delete(Bill bill);

    @Update
    int update(Bill bill);
}
