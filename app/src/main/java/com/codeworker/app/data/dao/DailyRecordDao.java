package com.codeworker.app.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.codeworker.app.data.entity.DailyRecord;

import java.util.List;

@Dao
public interface DailyRecordDao {
    @Query("SELECT * FROM daily_record_table WHERE month = :month and year = :year ORDER BY day DESC")
    List<DailyRecord> getDailyRecordListByMonth(int month, int year);

    @Query("SELECT * FROM daily_record_table WHERE day = :day and month = :month and year = :year")
    DailyRecord getDailyRecordByDMY(int day, int month, int year);

    @Query("SELECT * FROM daily_record_table WHERE day = :day and month = :month and year = :year")
    LiveData<DailyRecord> getDailyRecordDataByDMY(int day, int month, int year);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DailyRecord dailyRecord);

    @Update
    void update(DailyRecord dailyRecord);
}
