package com.codeworker.app.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.codeworker.app.data.entity.MonthlyRecord;

import java.util.List;

@Dao
public interface MonthlyRecordDao {
    @Query("SELECT * FROM monthly_record_table WHERE year = :year ORDER BY month ASC")
    List<MonthlyRecord> getMonthlyRecordListByYear(int year);

    @Query("SELECT * FROM monthly_record_table WHERE year = :year and month = :month")
    LiveData<MonthlyRecord> getMonthlyRecordDataByMonthAndYear(int month, int year);

    @Query("SELECT * FROM monthly_record_table WHERE year = :year and month = :month")
    MonthlyRecord getMonthlyRecordByMonthAndYear(int month, int year);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MonthlyRecord monthlyRecord);

    @Update
    void update(MonthlyRecord monthlyRecord);
}
