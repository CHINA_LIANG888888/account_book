package com.codeworker.app.data.entity;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;

/**
 * 每月收支记录表
 */
@Entity(tableName = "monthly_record_table")
public class MonthlyRecord extends Record {
    @RequiresApi(api = Build.VERSION_CODES.O)
    public MonthlyRecord() {
        super();
    }

    @NonNull
    @Override
    public String toString() {
        return "MonthlyRecord{" +
                "id=" + id +
                ", month=" + month +
                ", income=" + income.doubleValue() /100 +
                ", expend=" + expend.doubleValue() /100 +
                ", year='" + year + '\'' +
                '}';
    }
}
