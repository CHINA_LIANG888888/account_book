package com.codeworker.app.data.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.codeworker.app.data.common.Constant;

import java.io.Serializable;

/**
 * 资产表
 */
@Entity(tableName = "property_table")
public class Property implements Serializable {
    /**
     * 主键
     */
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    /**
     * 账户名称
     */
    private String name;

    /**
     * 账户资产，以分为单位
     */
    private Integer value;

    /**
     * 资产备注
     */
    private String remark;

    /**
     * 是否已注销账户，1为已注销，0为未注销
     */
    private Integer isDeleted;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Property property = (Property) o;

        if (!id.equals(property.id)) return false;
        if (!name.equals(property.name)) return false;
        if (!value.equals(property.value)) return false;
        if (remark != null ? !remark.equals(property.remark) : property.remark != null)
            return false;
        return isDeleted.equals(property.isDeleted);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + value.hashCode();
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + isDeleted.hashCode();
        return result;
    }

    public Property(@NonNull String name, int value, String remark) {
        this.name = name;
        this.value = value;
        this.isDeleted = Constant.NOT_DELETED;
        this.remark = remark;
    }

    @NonNull
    @Override
    public String toString() {
        return "Property{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value=" + value.doubleValue() / 100 +
                ", remark='" + remark + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }
}
