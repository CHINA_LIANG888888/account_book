package com.codeworker.app.data.common.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 记账类型枚举类
 */
public enum BillTypeEnum {
    EX_EAT(0, 0, "日常饮食", "ic_item_eat"),
    EX_CLOTHES(1, 0, "衣着服饰", "ic_item_clothes"),
    EX_RESIDENCE(2, 0, "居住租赁", "ic_item_house"),
    EX_LIVELIHOOD(3, 0, "生活用品", "ic_item_commodity"),
    EX_TRAFFIC(4, 0, "交通通信", "ic_item_traffic"),
    EX_EDUCATION(5, 0, "教育、文化和娱乐", "ic_item_edu"),
    EX_MEDICAL(6, 0, "医疗保健", "ic_item_medical"),
    EX_OTHERS(7, 0, "其他支出", "ic_item_other"),

    IN_SALARY(0, 1, "工资", "ic_item_salary"),
    IN_PART_TIME_JOB(1, 1, "兼职", "ic_item_part_time"),
    IN_INVESTMENT(2, 1, "理财", "ic_item_financial"),
    IN_CASH_GIFT(3, 1, "礼金", "ic_item_gift"),
    IN_OTHERS(4, 1, "其他收入", "ic_item_other_income")

    ;
    /**
     * 记账类型(数据库保存值)
     */
    private final Integer billTypeInDB;
    /**
     * 收支类型，1为收入，0为支出
     */
    private final Integer payType;
    /**
     * 记账类型(描述)
     */
    private final String billTypeDesc;
    /**
     * 字体图标编码
     */
    private final String billIconResId;

    public Integer getPayType() {
        return payType;
    }

    public Integer getBillTypeInDB() {
        return billTypeInDB;
    }

    public String getBillTypeDesc() {
        return billTypeDesc;
    }

    BillTypeEnum(Integer billTypeInDB, Integer payType, String billTypeDesc, String billIconResId) {
        this.billTypeInDB = billTypeInDB;
        this.payType = payType;
        this.billTypeDesc = billTypeDesc;
        this.billIconResId = billIconResId;
    }

    private static BillTypeEnum getBillTypeEnum(int payType, int billTypeInDB) {
        return Objects.requireNonNull(Arrays.stream(BillTypeEnum.values())
                .filter(v -> v.payType == payType && v.billTypeInDB == billTypeInDB)
                .findFirst()
                .orElse(null));
    }

    public static String getBillDesc(int payType, int billTypeInDB) {
        return getBillTypeEnum(payType, billTypeInDB).billTypeDesc;
    }

    public static String getBillIcon(int payType, int billTypeInDB) {
        return getBillTypeEnum(payType, billTypeInDB).billIconResId;
    }

    public static List<BillTypeEnum> getBillTypeValueByPayType(int payType) {
        return Objects.requireNonNull(Arrays.stream(BillTypeEnum.values())
                .filter(v -> v.getPayType().equals(payType))
                .collect(Collectors.toList()));
    }
}
