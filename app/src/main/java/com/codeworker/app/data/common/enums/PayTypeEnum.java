package com.codeworker.app.data.common.enums;

public enum PayTypeEnum {
    INCOME(1, "收入"),
    EXPEND(0, "支出"),
    ;
    /**
     * 收支类型，1为收入，0为支出
     */
    private final Integer payTypeInDB;
    /**
     * 收支类型描述
     */
    private final String payTypeDesc;

    PayTypeEnum(Integer payTypeInDB, String payTypeDesc) {
        this.payTypeInDB = payTypeInDB;
        this.payTypeDesc = payTypeDesc;
    }

    public Integer getPayTypeInDB() {
        return payTypeInDB;
    }

    public String getPayTypeDesc() {
        return payTypeDesc;
    }

    public static Integer getPayTypeInDB(String payTypeDesc){
        return INCOME.payTypeDesc.equals(payTypeDesc) ? INCOME.payTypeInDB : EXPEND.payTypeInDB;
    }

    public static String getPayTypeDesc(int payTypeInDB){
        return INCOME.payTypeInDB == payTypeInDB ? INCOME.payTypeDesc : EXPEND.payTypeDesc;
    }

}
