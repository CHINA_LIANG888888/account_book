package com.codeworker.app.data.entity.vo;

import androidx.annotation.NonNull;

import com.codeworker.app.data.entity.Bill;
import com.codeworker.app.data.entity.DailyRecord;

import java.io.Serializable;
import java.util.List;

public class BillData implements Serializable {

    private DailyRecord dailyRecord;
    private List<Bill> dailyBills;
    private boolean showDailyBills = true;

    @NonNull
    @Override
    public String toString() {
        return "BillData{" +
                "dailyRecord=" + dailyRecord +
                ", dailyBills=" + dailyBills +
                ", showDailyBills=" + showDailyBills +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BillData billData = (BillData) o;

        if (showDailyBills != billData.showDailyBills) return false;
        if (dailyRecord != null ? !dailyRecord.equals(billData.dailyRecord) : billData.dailyRecord != null)
            return false;
        return dailyBills != null ? dailyBills.equals(billData.dailyBills) : billData.dailyBills == null;
    }

    @Override
    public int hashCode() {
        int result = dailyRecord != null ? dailyRecord.hashCode() : 0;
        result = 31 * result + (dailyBills != null ? dailyBills.hashCode() : 0);
        result = 31 * result + (showDailyBills ? 1 : 0);
        return result;
    }

    public DailyRecord getDailyRecord() {
        return dailyRecord;
    }

    public void setDailyRecord(DailyRecord dailyRecord) {
        this.dailyRecord = dailyRecord;
    }

    public List<Bill> getDailyBills() {
        return dailyBills;
    }

    public void setDailyBills(List<Bill> dailyBills) {
        this.dailyBills = dailyBills;
    }

    public boolean isShowDailyBills() {
        return showDailyBills;
    }

    public void setShowDailyBills(boolean showDailyBills) {
        this.showDailyBills = showDailyBills;
    }
}
