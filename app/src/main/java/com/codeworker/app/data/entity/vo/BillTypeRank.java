package com.codeworker.app.data.entity.vo;

import androidx.annotation.NonNull;

import com.codeworker.app.utils.FormatUtil;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * 收支具体类别占比类
 */
public class BillTypeRank implements Serializable {
    /**
     * 使用颜色
     */
    private String billTypeColor;
    /**
     * 收支具体类别
     */
    private String billType;
    /**
     * 类别占比
     */
    private String proportion;
    /**
     * 金额
     */
    private String money;
    /**
     * 笔数
     */
    private Integer times;

    public BillTypeRank(String billTypeColor, String billType, Double proportion, Integer money, Integer times) {
        DecimalFormat df = new DecimalFormat( "0.00");
        this.billTypeColor = billTypeColor;
        this.billType = billType;
        this.proportion = df.format(proportion*100);
        this.money = FormatUtil.format(money*1.0/100);
        this.times = times;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BillTypeRank that = (BillTypeRank) o;

        if (!billTypeColor.equals(that.billTypeColor)) return false;
        if (!billType.equals(that.billType)) return false;
        if (!proportion.equals(that.proportion)) return false;
        if (!money.equals(that.money)) return false;
        return times.equals(that.times);
    }

    @Override
    public int hashCode() {
        int result = billTypeColor.hashCode();
        result = 31 * result + billType.hashCode();
        result = 31 * result + proportion.hashCode();
        result = 31 * result + money.hashCode();
        result = 31 * result + times.hashCode();
        return result;
    }

    @NonNull
    @Override
    public String toString() {
        return "BillTypeRank{" +
                "billTypeColor='" + billTypeColor + '\'' +
                ", billType='" + billType + '\'' +
                ", proportion=" + proportion +
                ", money=" + money +
                ", times=" + times +
                '}';
    }

    public String getBillTypeColor() {
        return billTypeColor;
    }

    public void setBillTypeColor(String billTypeColor) {
        this.billTypeColor = billTypeColor;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getProportion() {
        return proportion;
    }

    public void setProportion(String proportion) {
        this.proportion = proportion;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }
}
