package com.codeworker.app.data.model;

import android.app.Application;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.codeworker.app.data.entity.Property;
import com.codeworker.app.data.repository.PropertyRepository;

import java.util.List;

public class PropertyModel extends AndroidViewModel {

    private final PropertyRepository propertyRepository;

    private final LiveData<List<Property>> propertyListLiveData;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public PropertyModel(Application application) {
        super(application);
        propertyRepository = new PropertyRepository(application);
        propertyListLiveData = propertyRepository.getPropertyList();
    }

    public List<Property> getPropertyList() {
        return propertyRepository.getProperties();
    }

    public LiveData<List<Property>> getPropertyListLiveData() {
        return propertyListLiveData;
    }

    public void insert(Property property) {
        propertyRepository.insert(property);
    }

    public void softDelete(int propertyId) {
        propertyRepository.softDelete(propertyId);
    }

    public void repay(Integer inId, Integer outId, Integer value){
        propertyRepository.repay(inId, outId, value);
    }

}