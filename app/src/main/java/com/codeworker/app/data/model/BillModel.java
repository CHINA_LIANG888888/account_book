package com.codeworker.app.data.model;

import android.app.Application;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.codeworker.app.data.entity.Bill;
import com.codeworker.app.data.entity.DailyRecord;
import com.codeworker.app.data.entity.MonthlyRecord;
import com.codeworker.app.data.repository.BillRepository;

import java.util.List;

public class BillModel extends AndroidViewModel {

    private final BillRepository billRepository;

    public LiveData<List<Bill>> getBillListLiveData() {
        return billListLiveData;
    }

    public void setBillListLiveData(int day, int month, int year) {
        this.billListLiveData = billRepository.getDailyBillList(day, month, year);
    }

    private LiveData<List<Bill>> billListLiveData;

    public LiveData<DailyRecord> getDailyRecordLiveData() {
        return dailyRecordLiveData;
    }

    public void setDailyRecordLiveData(int day, int month, int year) {
        this.dailyRecordLiveData = billRepository.getDailyRecordData(day, month, year);
    }

    private LiveData<DailyRecord> dailyRecordLiveData;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public BillModel(Application application) {
        super(application);
        billRepository = new BillRepository(application);
        monthlyRecordDataList = billRepository.getMonthlyRecord();
    }

    public void setMonthlyRecordDataList(int month, int year) {
        this.monthlyRecordDataList = billRepository.getMonthlyRecordData(month, year);
    }

    public LiveData<MonthlyRecord> getMonthlyRecordDataList() {
        return monthlyRecordDataList;
    }

    private LiveData<MonthlyRecord> monthlyRecordDataList;


}
