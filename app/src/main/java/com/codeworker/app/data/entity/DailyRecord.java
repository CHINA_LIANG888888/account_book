package com.codeworker.app.data.entity;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;

/**
 * 每日收支记录表
 */
@Entity(tableName = "daily_record_table")
public class DailyRecord extends Record {
    @RequiresApi(api = Build.VERSION_CODES.O)
    public DailyRecord() {
        super();
    }

    @NonNull
    @Override
    public String toString() {
        return "DailyRecord{" +
                "id=" + id +
                ", day=" + day +
                ", income=" + income.doubleValue() /100 +
                ", expend=" + expend.doubleValue() /100 +
                ", month=" + month +
                ", year=" + year +
                '}';
    }
}
