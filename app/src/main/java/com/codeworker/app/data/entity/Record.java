package com.codeworker.app.data.entity;

import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.time.LocalDate;

public class Record implements Serializable {
    /**
     * 主键
     */
    @PrimaryKey(autoGenerate = true)
    protected Integer id;

    /**
     * 统计日
     */
    protected Integer day;

    /**
     * 统计月
     */
    protected Integer month;

    /**
     * 统计年
     */
    protected Integer year;

    /**
     * 统计收入，以分为单位
     */
    protected Integer income;

    /**
     * 统计支出，以分为单位
     */
    protected Integer expend;

    public Record() {
        LocalDate localDate = LocalDate.now();
        this.day = localDate.getDayOfMonth();
        this.month = localDate.getMonthValue();
        this.year = localDate.getYear();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public Integer getExpend() {
        return expend;
    }

    public void setExpend(Integer expend) {
        this.expend = expend;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (id != null ? !id.equals(record.id) : record.id != null) return false;
        if (day != null ? !day.equals(record.day) : record.day != null) return false;
        if (month != null ? !month.equals(record.month) : record.month != null) return false;
        if (year != null ? !year.equals(record.year) : record.year != null) return false;
        if (income != null ? !income.equals(record.income) : record.income != null) return false;
        return expend != null ? expend.equals(record.expend) : record.expend == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (day != null ? day.hashCode() : 0);
        result = 31 * result + (month != null ? month.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (income != null ? income.hashCode() : 0);
        result = 31 * result + (expend != null ? expend.hashCode() : 0);
        return result;
    }
}




